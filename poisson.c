/**
* @file poisson.c
* @author Thomas Lextrait, tlextrait@wpi.edu
*/

#include "poisson.h"

/**
 * returns a poisson distributed random number
 */
long int getRandomPoisson(int mean)
{
  /*
   * This algoritm was provided by Knuth
   */

  if(mean < 6){
    return mean;
  }else{
    long double p = 1.0;
    long double L = exp(-(long double)(mean));
    long int k = 0;
    
    while(p > L && k < 5*mean){
      k++;
      p *= drand48();
    }
    
    return k - 1;
  }
}
