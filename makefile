#######################################
# Thomas Lextrait , tlextrait@wpi.edu
#######################################
CC=gcc
CFLAGS=-c -Wall
THREAD=-pthread
MATH=-lm
#######################################
all: bathroomSim

bathroomSim: bathroomSim.o bathroom.o poisson.o
	$(CC) $(THREAD) $(MATH) bathroomSim.o bathroom.o poisson.o -o bathroomSim

bathroomSim.o: bathroomSim.c bathroomSim.h bathroom.h poisson.h
	$(CC) $(CFLAGS) bathroomSim.c

bathroom.o: bathroom.c bathroom.h
	$(CC) $(CFLAGS) bathroom.c

poisson.o: poisson.c poisson.h
	$(CC) $(CFLAGS) poisson.c

clean:
	rm -f *.o bathroom bathroomSim

superclean:
	rm -f *.o bathroom bathroomSim *~
