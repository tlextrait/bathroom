/**
* @file bathroomSim.h
* @author Thomas Lextrait, tlextrait@wpi.edu
*/

#include <stdlib.h>             // exit, malloc...
#include <stdio.h>              // printf, files...
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>             // usleep
#include <sys/syscall.h>
#include <errno.h>

/**
 * Structure for passing arguments to a thread
 */
struct Thread_Data{
  int thread_id;
  enum GENDER g;
  int meanArrival;
  int meanStay;
  int loopCount;
  long int minStayQueue; // filled out by thread after completing
  long int maxStayQueue; // filled out by thread after completing
  long int avgStayQueue; // filled out by thread after completing
  long int *queueStays; // list of all queue wait times
};

/**
 * Function executed by the threads
 */
void *Individual(void *thread_args);

/**
 * Populates the thread_data data structure for a given thread
 * creating a random gender and random loop count
 */
void populateData(struct Thread_Data *data, int thread_id, int meanArrival, int meanStay, int meanLoopCount);

/**
 * Validates the given arguments and copies them
 */
int validateArgs(char** args, int* nUsers, int* meanArrival, int* meanStay, int* meanLoopCount);

/**
 * printm allows a thread to print to screen with mutex protection
 */
void printm(struct Thread_Data data);
