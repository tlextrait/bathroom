/** 
 * @file bathroom.c
 * @author Thomas Lextrait, tlextrait@wpi.edu
 */

#include "bathroom.h"

long int EnterBathroom(enum GENDER g, struct Bathroom *bathroom)
{
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);
  
  // Bathroom occupied by other gender
  if(bathroom->gender != g && bathroom->occupied==0){
    // Lock and wait until signal
    pthread_mutex_lock(&(bathroom->mutex));
    pthread_cond_wait(&(bathroom->cond), &(bathroom->mutex));
    
    // Now enter
    bathroom->occupied = 0;
    bathroom->occupants = 1;
    bathroom->gender = g;
    
    pthread_mutex_unlock(&(bathroom->mutex));
  }else if(bathroom->occupied != 0){
    // Bathroom was vacant
    pthread_mutex_lock(&(bathroom->mutex));
    bathroom->occupied = 0;
    bathroom->occupants = 1;
    bathroom->gender = g;
    pthread_mutex_unlock(&(bathroom->mutex));
  }
  else if(bathroom->occupied == 0 && bathroom->gender==g){
    // Bathroom is occupied by same gender
    bathroom->occupants++;
  }
  
  gettimeofday(&tv2, NULL);
  if(tv1.tv_usec > tv2.tv_usec){
    tv2.tv_sec--;
    tv2.tv_usec += 1000000;
  }
  
  return (tv2.tv_usec - tv1.tv_usec) + (tv2.tv_sec - tv1.tv_sec)*1000000;
}

void LeaveBathroom(struct Bathroom *bathroom)
{
  
  pthread_mutex_lock(&(bathroom->mutex));
  
  // Bathroom is still occupied
  if(bathroom->occupied == 0){
    bathroom->occupants--;
    
    // Bathroom empty?
    if(bathroom->occupants <= 0){
      bathroom->gender = none;
      bathroom->occupied = -1;
      // Signal that the bathroom is vacated
      pthread_cond_broadcast(&(bathroom->cond));
    }
  }
  
  pthread_mutex_unlock(&(bathroom->mutex));
}

void Initialize(struct Bathroom *bathroom){
  bathroom->gender = none;      // no gender
  bathroom->occupants = 0;      // zero occupants
  bathroom->occupied = -1;      // not occupied
  
  // Initialize mutex and condition
  pthread_mutex_init(&(bathroom->mutex), NULL);
  pthread_cond_init(&(bathroom->cond), NULL);
}

void printBathroom(struct Bathroom *bathroom){
  
  char *gender;
  if(bathroom->gender == male)          {gender="M";}
  if(bathroom->gender == female)        {gender="F";}
  if(bathroom->gender == none)          {gender="V";}
  
  printf("----------------------------\n");
  printf("Occupants: %d; Gender: %s\n", bathroom->occupants, gender);
}
