/** 
 * @file bathroomSim.c
 * @author Thomas Lextrait, tlextrait@wpi.edu
 */

#include "bathroom.h"
#include "bathroomSim.h"
#include "poisson.h"

#define MAX_THREADS 300
#define MAX_THREAD_LOOPS 10000

/**
 * Global variables 
 */
pthread_t               threads[MAX_THREADS];      // thread array
struct Thread_Data      thread_data[MAX_THREADS];  // thread data array
pthread_cond_t          sameSexCond;               // same-sex Mutex condition
struct Bathroom         *bathroom;                 // bathroom Object
pthread_mutex_t         mutexPrint;                // printing Mutex

/**
 * Main program
 */
int main(int argc, char **argv)
{
  
  int rc, i, nUsers, meanArrival, meanStay, meanLoopCount;
  bathroom = malloc(sizeof(struct Bathroom));    // General shared bathroom structure
  void *status;                 // Collect status information when joining threads
  pthread_attr_t attr;          // thread attributes
  
  if(argc!=5){
    
    printf("bathroomSim takes 4 parameters:\n");
    printf("./bathroomSim [nUsers] [meanArrival] [meanStay] [meanLoopCount]\n");
    exit(1);
    
  }else{

    if(validateArgs(argv, &nUsers, &meanArrival, &meanStay, &meanLoopCount) != 0){
      printf("Given numbers are invalid, they should all be positive.\n");
      exit(1);
    }else{
      
      if(nUsers > MAX_THREADS){
        nUsers = MAX_THREADS;
        printf("Warning: maximum threads is %d. Only this number of threads will be spawned.\n", MAX_THREADS);
      }
      
      if(meanLoopCount > MAX_THREAD_LOOPS){
        meanLoopCount = MAX_THREAD_LOOPS;
        printf("Warning: maximum loop count is %d. It is the maximum number of loops the threads will perform.\n", MAX_THREAD_LOOPS);
      }
      
      // Initialize
      pthread_attr_init(&attr);
      pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
      pthread_mutex_init(&mutexPrint, NULL);
      pthread_cond_init(&sameSexCond, NULL);
      Initialize(bathroom);
      
      srand(time(NULL));
      srand48(time(NULL));
      
      // Create all the individuals
      for(i=0; i<nUsers; i++){
        
        populateData(&thread_data[i], i, meanArrival, meanStay, meanLoopCount);
        rc = pthread_create(&threads[i], &attr, Individual, (void*) &thread_data[i]);
        
        if(rc){
          printf("Error: pthread_create() failed and returned %d\n", rc);
          exit(1);
        }
      }
      
      // Join all the threads
      for(i=0; i<nUsers; i++){
        rc = pthread_join(threads[i], &status);
        if(rc){
          printf("Error: pthread_join() failed and returned %d\n", rc);
          exit(1);
        }
      }
      
      printf("All threads have joined, all individuals have exitted the bathroom.\n");
      
      // Clean up
      pthread_mutex_destroy(&(bathroom->mutex));
      pthread_mutex_destroy(&mutexPrint);
      pthread_cond_destroy(&sameSexCond);
      pthread_cond_destroy(&(bathroom->cond));
      pthread_attr_destroy(&attr);
      pthread_exit(NULL);
    }
  }
}

/**
 * Populates the thread_data data structure for a given thread
 * creating a random gender and random loop count
 */
void populateData(struct Thread_Data *data, int thread_id, int meanArrival, int meanStay, int meanLoopCount){
  int r, loopCount;
  enum GENDER randomGender;
  
  // Create random gender
  r = (int)(drand48()*10)%2;
  if(r==1){randomGender=male;}else{randomGender=female;}
  
  // Create random loop count
  loopCount = getRandomPoisson(meanLoopCount);
  
  data->thread_id = thread_id;
  data->g = randomGender;
  data->meanArrival = meanArrival;
  data->meanStay = meanStay;
  data->loopCount = loopCount;
  
  data->minStayQueue = -1;
  data->maxStayQueue = -1;
  data->avgStayQueue = 0;
  
  data->queueStays = malloc(sizeof(long int)*loopCount+1);
}

/**
 * Function executed by the threads
 */
void *Individual(void *thread_args)
{
  
  int i;
  long int queueTime;
  
  // Copy given data
  struct Thread_Data *data;
  data = (struct Thread_Data *) thread_args;
  
  // Try to enter the bathroom for loopcount
  for(i=0; i<(data->loopCount); i++){
    
    // Wait before trying to enter
    usleep(getRandomPoisson(data->meanArrival));
    
    queueTime = EnterBathroom(data->g, bathroom);
    usleep(getRandomPoisson(data->meanStay));
    LeaveBathroom(bathroom);
    
    // Stats
    if(data->minStayQueue > queueTime || data->minStayQueue < 0){data->minStayQueue = queueTime;}
    if(data->maxStayQueue < queueTime || data->maxStayQueue < 0){data->maxStayQueue = queueTime;}
    data->queueStays[i] = queueTime;
    data->avgStayQueue += queueTime;
  }
  
  // Calculate my stats
  data->avgStayQueue /= data->loopCount;
  
  // Done! now print my info with mutex protection
  printm(*data);
  
  // Kill thread
  pthread_exit((void*) 0);
}

/**
 * printm allows a thread to print to screen with mutex protection
 */
void printm(struct Thread_Data data)
{
  pthread_mutex_lock(&mutexPrint);
  
  printf("Thread %3i\t", data.thread_id);
  if(data.g == male){printf("MALE\t| ");}else{printf("FEMALE\t| ");}
  printf("loops: %4i | minQueue: %8li usec | maxQueue: %8li usec | avgQueue: %8li usec\n", data.loopCount, data.minStayQueue, data.maxStayQueue, data.avgStayQueue);
  
  pthread_mutex_unlock(&mutexPrint);
}

/**
 * Validates the given arguments and copies them
 */
int validateArgs(char** args, int* nUsers, int* meanArrival, int* meanStay, int* meanLoopCount)
{
  // Copy the values
  *nUsers = atoi(args[1]);
  *meanArrival = atoi(args[2]);
  *meanStay = atoi(args[3]);
  *meanLoopCount = atoi(args[4]);
  
  // Check all the values are positive
  if(*nUsers < 0 || *meanArrival < 0 || *meanStay < 0 || *meanLoopCount < 0){
    return -1;
  }else{
    return 0;
  }
  
}
