/**
* @file bathroom.h
* @author Thomas Lextrait, tlextrait@wpi.edu
*/

#include <stdlib.h>             // exit, malloc...
#include <stdio.h>              // printf, files...
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

enum GENDER {male, female, none};

struct Bathroom{
  enum GENDER gender;   // occupying gender, null if none
  int occupied;         // -1 if not, 0 if occupied
  int occupants;        // number of occupants
  pthread_mutex_t mutex;   // bathroom Mutex
  pthread_cond_t  cond;    // same-sex Mutex condition
};

/**
 * Enter the bathroom but wait until vacant if
 * occupied by the opposite gender. Set state
 * accordingly
 * @return total queue waiting time
 */
long int EnterBathroom(enum GENDER g, struct Bathroom *bathroom);

/**
 * Leave the bathroom. Set the state to "vacant" if
 * this thread is the last one out
 */
void LeaveBathroom(struct Bathroom *bathroom);

/**
 * Initialize the bathroom
 */
void Initialize(struct Bathroom *bathroom);

/**
 * Prints the representation of a bathroom with its data
 */
void printBathroom(struct Bathroom *bathroom);
