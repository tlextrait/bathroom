/**
* @file poisson.h
* @author Thomas Lextrait, tlextrait@wpi.edu
*/

#include <stdlib.h>             // exit, malloc...
#include <stdio.h>              // printf, files...
#include <math.h>
#include <pthread.h>

/**
 * returns a poisson distributed random number
 */
long int getRandomPoisson(int mean);
